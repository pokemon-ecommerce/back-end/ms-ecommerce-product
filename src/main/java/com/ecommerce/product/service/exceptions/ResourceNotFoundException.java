package com.ecommerce.product.service.exceptions;

/**
 * <b>Class</b>: ResourceNotFoundException <br/>
 * .
 *
 * @author Carlos <br/>
 */

public class ResourceNotFoundException extends RuntimeException {

  private static final String MESSAGE = "Could not find resource %s";

  public ResourceNotFoundException(Long id) {
    super(String.format(MESSAGE, String.valueOf(id)));
  }

  public ResourceNotFoundException(String id) {
    super(String.format(MESSAGE, id));
  }

}
