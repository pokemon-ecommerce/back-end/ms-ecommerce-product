package com.ecommerce.product.service.exceptions;

/**
 * <b>Class</b>: BusinessException <br/>
 * .
 *
 * @author Carlos <br/>
 */
public class BusinessException extends RuntimeException {
  public BusinessException(final String message) {
    super(message);
  }
}
