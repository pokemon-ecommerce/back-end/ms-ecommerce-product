package com.ecommerce.product.service.service.impl;

import com.ecommerce.product.service.exceptions.BusinessException;
import com.ecommerce.product.service.exceptions.ResourceNotFoundException;
import com.ecommerce.product.service.mappers.ProductMapper;
import com.ecommerce.product.service.model.dto.ProductDto;
import com.ecommerce.product.service.model.dto.ProductResponseDto;
import com.ecommerce.product.service.model.dto.StockProductDto;
import com.ecommerce.product.service.repository.ProductRepository;
import com.ecommerce.product.service.service.ProductService;
import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: ProductServiceImpl <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  @Override
  public Mono<ProductResponseDto> findAllProducts(final Integer page, final Integer size) {
    return Mono.fromCallable(() -> productRepository.findAll(PageRequest.of(page, size)))
        .map(ProductMapper::productPageToProductResponseDto);
  }

  @Override
  public Mono<List<ProductDto>> findAllProducts() {
    return Mono.fromCallable(productRepository::findAll)
        .map(products -> products.stream()
            .map(ProductMapper::productToProductDto)
            .collect(Collectors.toList()));
  }

  @Override
  public Mono<ProductDto> findProductById(final Long id) {
    return productRepository.findById(id)
        .map(ProductMapper::productToProductDto)
        .map(Mono::just)
        .orElseThrow(() -> new ResourceNotFoundException(id));
  }

  @Override
  public Mono<ProductDto> saveProduct(final ProductDto productDto) {
    return Mono.fromCallable(() -> ProductMapper.productDtoToProduct(productDto))
        .map(productRepository::save)
        .map(ProductMapper::productToProductDto)
        .onErrorMap(error -> new BusinessException(error.getMessage()));
  }

  @Override
  public Mono<ProductDto> updateProduct(final ProductDto productDto, final Long id) {
    return productRepository.findById(id)
        .map(product -> ProductMapper.productDtoToProduct(productDto))
        .map(productRepository::save)
        .map(ProductMapper::productToProductDto)
        .map(Mono::just)
        .orElseThrow(() -> new ResourceNotFoundException(id));
  }

  @Override
  public Mono<ProductDto> updateStockProduct(final StockProductDto stockProductDto, Long id) {
    return productRepository.findById(id)
        .map(product -> {
          product.setStock(product.getStock() - stockProductDto.getStock());
          return product;
        })
        .map(productRepository::save)
        .map(ProductMapper::productToProductDto)
        .map(Mono::just)
        .orElseThrow(() -> new ResourceNotFoundException(id));
  }

  @Override
  public Mono<ProductDto> deleteProductById(Long id) {
    return productRepository.findById(id)
        .map(product -> {
          productRepository.deleteById(id);
          return Mono.fromCallable(() -> ProductMapper.productToProductDto(product));
        })
        .orElseThrow(() -> new ResourceNotFoundException(id));
  }

}
