package com.ecommerce.product.service.service;

import com.ecommerce.product.service.model.dto.ProductDto;
import com.ecommerce.product.service.model.dto.ProductResponseDto;
import com.ecommerce.product.service.model.dto.StockProductDto;

import java.util.List;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: ProductService <br/>
 * .
 *
 * @author Carlos <br/>
 */
public interface ProductService {

  Mono<ProductResponseDto> findAllProducts(Integer page, Integer size);

  Mono<List<ProductDto>> findAllProducts();

  Mono<ProductDto> findProductById(Long id);

  Mono<ProductDto> saveProduct(ProductDto productDto);

  Mono<ProductDto> updateProduct(ProductDto productDto, Long id);

  Mono<ProductDto> updateStockProduct(StockProductDto stockProductDto, Long id);

  Mono<ProductDto> deleteProductById(Long id);

}
