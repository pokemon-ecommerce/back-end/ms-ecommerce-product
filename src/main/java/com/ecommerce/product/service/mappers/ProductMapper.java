package com.ecommerce.product.service.mappers;

import com.ecommerce.product.service.model.dto.ProductDto;
import com.ecommerce.product.service.model.dto.ProductResponseDto;
import com.ecommerce.product.service.model.entity.Product;

import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import org.springframework.data.domain.Page;

/**
 * <b>Class</b>: ProductMapper <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ProductMapper {

  /**
   * productToProductDto .
   *
   * @param product .
   */
  public static ProductDto productToProductDto(final Product product) {
    return ProductDto.builder()
        .id(product.getId())
        .name(product.getName())
        .description(product.getDescription())
        .price(product.getPrice())
        .stock(product.getStock())
        .image(product.getImage())
        .build();
  }

  /**
   * productDtoToProduct .
   *
   * @param productDto .
   * @return
   */
  public static Product productDtoToProduct(final ProductDto productDto) {
    return Product.builder()
        .id(productDto.getId())
        .name(productDto.getName())
        .description(productDto.getDescription())
        .price(productDto.getPrice())
        .stock(productDto.getStock())
        .image(productDto.getImage())
        .build();
  }

  /**
   * productPageToProductResponseDto .
   *
   * @param productPage .
   * @return
   */
  public static ProductResponseDto productPageToProductResponseDto(Page<Product> productPage) {
    return ProductResponseDto.builder()
        .currentPage(productPage.getNumber())
        .totalItems(productPage.getTotalElements())
        .totalPages(productPage.getTotalPages())
        .products(productPage.getContent()
            .stream()
            .map(ProductMapper::productToProductDto)
            .collect(Collectors.toList()))
        .build();
  }

}
