package com.ecommerce.product.service.controller.advice;

import com.ecommerce.product.service.exceptions.BusinessException;
import com.ecommerce.product.service.exceptions.ResourceNotFoundException;
import com.ecommerce.product.service.model.dto.ErrorResponseDto;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: ProductAdvice <br/>
 * .
 *
 * @author Carlos <br/>
 */
@ControllerAdvice
public class ProductAdvice {

  /**
   * resourceNotFoundException
   * .
   *
   * @param exception .
   * @return
   */
  @ResponseBody
  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public Mono<ErrorResponseDto> resourceNotFoundException(final ResourceNotFoundException exception) {
    return Mono.just(ErrorResponseDto.builder()
        .message(exception.getMessage())
        .build());
  }

  /**
   * businessException .
   *
   * @param exception .
   * @return
   */
  @ResponseBody
  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public Mono<ErrorResponseDto> businessException(final BusinessException exception) {
    return Mono.just(ErrorResponseDto.builder()
        .message(exception.getMessage())
        .build());
  }

}
