package com.ecommerce.product.service.controller;

import com.ecommerce.product.service.model.dto.ProductDto;
import com.ecommerce.product.service.model.dto.ProductResponseDto;
import com.ecommerce.product.service.model.dto.StockProductDto;
import com.ecommerce.product.service.service.impl.ProductServiceImpl;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

/**
 * <b>Class</b>: ProductController <br/>
 * .
 *
 * @author Carlos <br/>
 */
@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

  private final ProductServiceImpl productService;

  @GetMapping
  public Mono<ProductResponseDto> findAllProducts(@RequestParam(value = "page", defaultValue = "0") Integer page,
      @RequestParam(value = "size", defaultValue = "10") Integer size) {
    return productService.findAllProducts(page, size);
  }

  @GetMapping("/{id}")
  public Mono<ProductDto> findProductById(@PathVariable final Long id) {
    return productService.findProductById(id);
  }

  @PostMapping
  public Mono<ProductDto> saveProduct(@RequestBody final ProductDto productDto) {
    return productService.saveProduct(productDto);
  }

  @PutMapping("/{id}")
  public Mono<ProductDto> updateProduct(@RequestBody final ProductDto productDto, @PathVariable final Long id) {
    return productService.updateProduct(productDto, id);
  }

  @PatchMapping("/update-stock/{id}")
  public Mono<ProductDto> updateStockProduct(@RequestBody final StockProductDto stockProductDto,
      @PathVariable final Long id) {
    return productService.updateStockProduct(stockProductDto, id);
  }

  @DeleteMapping("/{id}")
  public Mono<ProductDto> deleteProductById(@PathVariable final Long id) {
    return productService.deleteProductById(id);
  }
}
