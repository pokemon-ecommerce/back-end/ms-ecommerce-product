package com.ecommerce.product.service.repository;

import com.ecommerce.product.service.model.entity.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <b>Class</b>: ProductRepository <br/>
 * .
 *
 * @author Carlos <br/>
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
