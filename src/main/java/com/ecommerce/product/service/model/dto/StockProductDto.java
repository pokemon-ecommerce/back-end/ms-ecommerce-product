package com.ecommerce.product.service.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: StockProductDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class StockProductDto {
  private Long id;
  private Integer stock;
}
