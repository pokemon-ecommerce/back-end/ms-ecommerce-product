package com.ecommerce.product.service.model.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: ErrorResponseDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class ErrorResponseDto {

  @Builder.Default
  private String id = UUID.randomUUID().toString();
  private String message;
}
