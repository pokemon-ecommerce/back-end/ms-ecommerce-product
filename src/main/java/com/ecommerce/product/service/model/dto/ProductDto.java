package com.ecommerce.product.service.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * <b>Class</b>: ProductDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
@ToString
public class ProductDto {
  private Long id;
  @NotNull
  private String name;
  private String description;
  private Double price;
  private Integer stock;
  private String image;
}
