package com.ecommerce.product.service.model.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <b>Class</b>: ProductResponseDto <br/>
 * .
 *
 * @author Carlos <br/>
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponseDto {
  private Long totalItems;
  private Integer totalPages;
  private Integer currentPage;
  private List<ProductDto> products;
}
